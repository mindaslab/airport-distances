
import pandas as pd
import numpy
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

def flight_distance(file_name,column_name_for_distance):
	url = 'https://www.world-airport-codes.com/distance/?a1=&a2=&code=IATA'
	df = pd.read_csv(file_name)
	df[column_name_for_distance] = 0
	df_o = df['origin']
	df_d = df['destination']
	df_dd = df[column_name_for_distance]
	length = len(df_o.index)	
	count = 0
	#chrome_options = webdriver.ChromeOptions()
	#chrome_options.add_argument("--incognito")
	#seleniumm web driver
	driver = webdriver.PhantomJS('/home/bala/Desktop/task/spider/phantomjs')
	for i in range(length):
		data_origin = df_o.iloc[i]
		data_destin = df_d.iloc[i]
		driver.implicitly_wait(30)
		driver.get(url)		
		#input for flight codes
		inputElement = driver.find_element_by_id("distance-a1-main")
		inputElement.send_keys(data_origin)
		inputElement = driver.find_element_by_id("distance-a2-main")
		inputElement.send_keys(data_destin)
		inputElement.send_keys(Keys.ENTER)
		soup = BeautifulSoup(driver.page_source, 'lxml')
		
		sss = soup.find("p")
		dd = sss.find("strong")
		distance = dd.contents[0]
		km = distance[:8]
		
		
		print(data_origin,'----->',data_destin,'---->',km)
		df[column_name_for_distance][count] = km
		count=count+1
		df.to_csv(file_name,index=False)
	driver.quit()

flight_distance('demo_unique_routes.csv','distance')
